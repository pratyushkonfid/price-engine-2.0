export const ZipCodeService = {
    extractZipCode: (string)=> {
        let zipRegexG = /[0-9]{5}(?:-[0-9]{4})?/g;
        return string.match(zipRegexG)[0];
    }
}